/*opam-version: "2.0"
  name: "stdlib-shims"
  version: "0.1.0"
  synopsis: "Backport some of the new stdlib features to older
  compiler"
  description: """
  Backport some of the new stdlib features to older compiler,
  such as the Stdlib module.
  
  This allows projects that require compatibility with older compiler to
  use these new features in their code."""
  maintainer: "The stdlib-shims programmers"
  authors: "The stdlib-shims programmers"
  license: "typeof OCaml system"
  tags: ["stdlib" "compatibility" "org:ocaml"]
  homepage: "https://github.com/ocaml/stdlib-shims"
  doc: "https://ocaml.github.io/stdlib-shims/"
  bug-reports: "https://github.com/ocaml/stdlib-shims/issues"
  depends: [
    "dune" {build}
    "ocaml" {>= "4.02.3"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/ocaml/stdlib-shims.git"
  url {
    src:
     
  "https://github.com/ocaml/stdlib-shims/releases/download/0.1.0/stdlib-shims-0.1.0.tbz"
    checksum: [
      "md5=12b5704eed70c6bff5ac39a16db1425d"
     
  "sha256=5373c987e9f82a4434fffa3eb7d795159138f8ffe76b786a1ac21877c8f266cb"
     
  "sha512=149d1746448724bd89535bff2118749102ea25b16942fc7165aab917bb60b97bd5d9e9b3e566a025bd0b599e7e607b43d9001113ef2eef687a6dfbab2788c759"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, dune, ocaml, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.1.0"; in
assert (vcompare ocaml "4.02.3") >= 0;

stdenv.mkDerivation rec {
  pname = "stdlib-shims";
  version = "0.1.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml/stdlib-shims/releases/download/0.1.0/stdlib-shims-0.1.0.tbz";
    sha256 = "1jv6yb47f66239m7hsz7zzw3i48mjpbvfgpszws48apqx63wjwsk";
  };
  buildInputs = [
    dune ocaml findlib ];
  propagatedBuildInputs = [
    ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
