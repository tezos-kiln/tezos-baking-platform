/*opam-version: "2.0"
  name: "num"
  version: "1.2"
  synopsis:
    "The legacy Num library for arbitrary-precision integer and rational
  arithmetic"
  maintainer: "Xavier Leroy <xavier.leroy@inria.fr>"
  authors: ["Valérie Ménissier-Morain" "Pierre Weis" "Xavier
  Leroy"]
  license: "LGPL 2.1 with OCaml linking exception"
  homepage: "https://github.com/ocaml/num/"
  bug-reports: "https://github.com/ocaml/num/issues"
  depends: [
    "ocaml" {>= "4.06.0"}
    "ocamlfind" {build & >= "1.7.3"}
  ]
  conflicts: ["base-num"]
  build: make
  install: [
    make
    "install" {!ocaml:preinstalled}
    "findlib-install" {ocaml:preinstalled}
  ]
  patches: "installation-warning.patch"
  dev-repo: "git+https://github.com/ocaml/num.git"
  extra-files: [
    "installation-warning.patch" "md5=93c92bf6da6bae09d068da42b1bbaaac"
  ]
  url {
    src: "https://github.com/ocaml/num/archive/v1.2.tar.gz"
    checksum: [
      "md5=4f43ce8e44db68692bee50f2f8ef911c"
     
  "sha256=c5023104925ff4a79746509d4d85294d8aafa98da6733e768ae53da0355453de"
     
  "sha512=cf6b5e026d82235e9ed2cc31cbe1bf29171964a364c6ff23aaf0cc864f646002ce2be5b7296488f1568c7b2166d298298cd086f26fb10b52863dbf78f8b23844"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.2"; in
assert (vcompare ocaml "4.06.0") >= 0;
assert (vcompare findlib "1.7.3") >= 0;

stdenv.mkDerivation rec {
  pname = "num";
  version = "1.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml/num/archive/v1.2.tar.gz";
    sha256 = "1pjkahss0gg5i9v3wwx6inlsz2jd562lv7ah8sbsgx2zj82320n5";
  };
  postUnpack = "ln -sv ${./installation-warning.patch} \"$sourceRoot\"/installation-warning.patch";
  buildInputs = [
    ocaml findlib ];
  propagatedBuildInputs = [
    ocaml findlib ];
  configurePhase = "true";
  patches = [
    "installation-warning.patch" ];
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" "'findlib-install'" ] ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
