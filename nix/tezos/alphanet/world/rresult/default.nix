/*opam-version: "2.0"
  name: "rresult"
  version: "0.6.0"
  synopsis: "Result value combinators for OCaml"
  description: """
  Rresult is an OCaml module for handling computation results and errors
  in an explicit and declarative manner, without resorting to
  exceptions. It defines combinators to operate on the `result` type
  available from OCaml 4.03 in the standard library.
  
  Rresult depends on the compatibility `result` package and is
  distributed under the ISC license."""
  maintainer: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  authors: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  license: "ISC"
  tags: ["result" "error" "declarative" "org:erratique"]
  homepage: "http://erratique.ch/software/rresult"
  doc: "http://erratique.ch/software/rresult"
  bug-reports: "https://github.com/dbuenzli/rresult/issues"
  depends: [
    "ocaml" {>= "4.01.0"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
    "topkg" {build}
    "result"
  ]
  build: ["ocaml" "pkg/pkg.ml" "build" "--pinned" "%{pinned}%"]
  dev-repo: "git+http://erratique.ch/repos/rresult.git"
  url {
    src: "http://erratique.ch/software/rresult/releases/rresult-0.6.0.tbz"
    checksum: [
      "md5=aba88cffa29081714468c2c7bcdf7fb1"
     
  "sha256=0645f65eae8f34e33b27a13da660e70b3814d099fb4b0e3513eeccbcdf50c9cc"
     
  "sha512=66cb7acb525c17dde4e7572fa5a0d925466f8e759b90a12d952ca076d25de49404c962e061557f2255112fbf1d731f8da696acd536cbb9433a4b1d9662095c3a"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, ocamlbuild, topkg, ocaml-result }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.6.0"; in
assert (vcompare ocaml "4.01.0") >= 0;

stdenv.mkDerivation rec {
  pname = "rresult";
  version = "0.6.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "http://erratique.ch/software/rresult/releases/rresult-0.6.0.tbz";
    sha256 = "1k69a3gvrk7f2cshwjzvk7818f0bwxhacgd14wxy6d4gmrggci86";
  };
  buildInputs = [
    ocaml findlib ocamlbuild topkg ocaml-result ];
  propagatedBuildInputs = [
    ocaml ocaml-result ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'ocaml'" "'pkg/pkg.ml'" "'build'" "'--pinned'" "false" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
