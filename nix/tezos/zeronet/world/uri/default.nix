/*opam-version: "2.0"
  name: "uri"
  version: "2.2.1"
  synopsis: "An RFC3986 URI/URL parsing library"
  description: """
  This is an OCaml implementation of the
  [RFC3986](http://tools.ietf.org/html/rfc3986) specification 
  for parsing URI or URLs."""
  maintainer: "anil@recoil.org"
  authors: ["Anil Madhavapeddy" "David Sheets" "Rudi Grinberg"]
  license: "ISC"
  tags: ["url" "uri" "org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-uri"
  doc: "https://mirage.github.io/ocaml-uri/"
  bug-reports: "https://github.com/mirage/ocaml-uri/issues"
  depends: [
    "ocaml" {>= "4.04.0"}
    "dune" {build & >= "1.2.0"}
    "ounit" {with-test & >= "1.0.2"}
    "ppx_sexp_conv" {build & >= "v0.9.0"}
    "re" {>= "1.9.0"}
    "sexplib0"
    "stringext" {>= "1.4.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-uri.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-uri/releases/download/v2.2.1/uri-v2.2.1.tbz"
    checksum: [
     
  "sha256=f6348edbae9b9011b3f65e85e775f2cf7b87735c5ceb54d6962377daec3bf56b"
     
  "sha512=28a4a659695d5d02f8a1b2965cbb05bc864bea5576c447707bb7db0e7411cafc78323ba6729e32b8b31fdc24a53e906274812d377e62829ebc9ebd6602331176"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, ounit ? null, ppx_sexp_conv, re, sexplib0,
  stringext, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "2.2.1"; in
assert (vcompare ocaml "4.04.0") >= 0;
assert (vcompare dune "1.2.0") >= 0;
assert doCheck -> (vcompare ounit "1.0.2") >= 0;
assert (vcompare ppx_sexp_conv "v0.9.0") >= 0;
assert (vcompare re "1.9.0") >= 0;
assert (vcompare stringext "1.4.0") >= 0;

stdenv.mkDerivation rec {
  pname = "uri";
  version = "2.2.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-uri/releases/download/v2.2.1/uri-v2.2.1.tbz";
    sha256 = "0szm7gndlxr3jvb59sswbirqfyygy9syg1ayysri344vmvdqwd7n";
  };
  buildInputs = [
    ocaml dune ounit ppx_sexp_conv re sexplib0 stringext findlib ];
  propagatedBuildInputs = [
    ocaml dune ounit ppx_sexp_conv re sexplib0 stringext ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
