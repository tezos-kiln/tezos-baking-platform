/*opam-version: "2.0"
  name: "macaddr"
  version: "3.1.0"
  synopsis: "A library for manipulation of MAC address
  representations"
  maintainer: "anil@recoil.org"
  authors: ["David Sheets" "Anil Madhavapeddy" "Hugo Heuzard"]
  license: "ISC"
  tags: ["org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-ipaddr"
  doc: "https://mirage.github.io/ocaml-ipaddr/"
  bug-reports: "https://github.com/mirage/ocaml-ipaddr/issues"
  depends: [
    "ocaml" {>= "4.04.0"}
    "dune" {build}
    "sexplib0"
    "ounit" {with-test}
    "ppx_sexp_conv" {with-test}
  ]
  conflicts: [
    "ipaddr" {< "3.0.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-ipaddr.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-ipaddr/releases/download/v3.1.0/ipaddr-v3.1.0.tbz"
    checksum: [
      "md5=471a594563bb9c3dd91ae912b5ffd6ed"
     
  "sha256=39bfbbe2d650c26be7b5581d288eaedd1b47b842cdebaa08d6afd968983fc3b4"
     
  "sha512=7db4ee264e256944b26116f42d8f022e1278318260890f73d5a267cb1d9a5c2a0cf8b6da1c0fccf1469cd0c7ddaafde9da17e921f00d6d44a2a41f1aa64a1a64"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, sexplib0, ounit ? null, ppx_sexp_conv ? null,
  findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "3.1.0"; in
assert (vcompare ocaml "4.04.0") >= 0;

stdenv.mkDerivation rec {
  pname = "macaddr";
  version = "3.1.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-ipaddr/releases/download/v3.1.0/ipaddr-v3.1.0.tbz";
    sha256 = "1d637yc6indgsq4amsyd8aw4f6yxms72h7aqnpknphjhsvibpgrr";
  };
  buildInputs = [
    ocaml dune sexplib0 ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  stdenv.lib.optional
  doCheck
  ppx_sexp_conv
  ++
  [
    findlib ];
  propagatedBuildInputs = [
    ocaml sexplib0 ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  stdenv.lib.optional
  doCheck
  ppx_sexp_conv;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
