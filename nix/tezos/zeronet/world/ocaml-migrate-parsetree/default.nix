/*opam-version: "2.0"
  name: "ocaml-migrate-parsetree"
  version: "1.4.0"
  synopsis: "Convert OCaml parsetrees between different versions"
  description: """
  Convert OCaml parsetrees between different versions
  
  This library converts parsetrees, outcometree and ast mappers
  between
  different OCaml versions.  High-level functions help making PPX
  rewriters independent of a compiler version."""
  maintainer: "frederic.bour@lakaban.net"
  authors: [
    "Frédéric Bour <frederic.bour@lakaban.net>"
    "Jérémie Dimino <jeremie@dimino.org>"
  ]
  license: "LGPL-2.1 with OCaml linking exception"
  tags: ["syntax" "org:ocamllabs"]
  homepage: "https://github.com/ocaml-ppx/ocaml-migrate-parsetree"
  doc: "https://ocaml-ppx.github.io/ocaml-migrate-parsetree/"
  bug-reports:
  "https://github.com/ocaml-ppx/ocaml-migrate-parsetree/issues"
  depends: [
    "result"
    "ppx_derivers"
    "dune" {build & >= "1.9.0"}
    "ocaml" {>= "4.02.3"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/ocaml-ppx/ocaml-migrate-parsetree.git"
  url {
    src:
     
  "https://github.com/ocaml-ppx/ocaml-migrate-parsetree/releases/download/v1.4.0/ocaml-migrate-parsetree-v1.4.0.tbz"
    checksum: [
     
  "sha256=231fbdc205187b3ee266b535d9cfe44b599067b2f6e97883c782ea7bb577d3b8"
     
  "sha512=61ee91d2d146cc2d2ff2d5dc4ef5dea4dc4d3c8dbd8b4c9586d64b6ad7302327ab35547aa0a5b0103c3f07b66b13d416a1bee6d4d117293cd3cabe44113ec6d4"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml-result, ppx_derivers, dune, ocaml, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.4.0"; in
assert (vcompare dune "1.9.0") >= 0;
assert (vcompare ocaml "4.02.3") >= 0;

stdenv.mkDerivation rec {
  pname = "ocaml-migrate-parsetree";
  version = "1.4.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-ppx/ocaml-migrate-parsetree/releases/download/v1.4.0/ocaml-migrate-parsetree-v1.4.0.tbz";
    sha256 = "1f6kfysppsl2qy1pisgnn9kr0nabwk7xjddmcvi3wyqq0p1bs7r3";
  };
  buildInputs = [
    ocaml-result ppx_derivers dune ocaml findlib ];
  propagatedBuildInputs = [
    ocaml-result ppx_derivers dune ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
