/*opam-version: "2.0"
  name: "conduit"
  version: "1.4.0"
  synopsis: "A network connection establishment library"
  description: """
  The `conduit` library takes care of establishing and listening for 
  TCP and SSL/TLS connections for the Lwt and Async libraries.
  
  The reason this library exists is to provide a degree of abstraction
  from the precise SSL library used, since there are a variety of ways
  to bind to a library (e.g. the C FFI, or the Ctypes library), as well
  as well as which library is used (just OpenSSL for now).
  
  By default, OpenSSL is used as the preferred connection library, but
  you can force the use of the pure OCaml TLS stack by setting the
  environment variable `CONDUIT_TLS=native` when starting your program.
  
  The useful opam packages available that extend this library are:
  
  - `conduit`: the main `Conduit` module
  - `conduit-lwt`: the portable Lwt implementation
  - `conduit-lwt-unix`: the Lwt/Unix implementation
  - `conduit-async` the Jane Street Async implementation
  - `mirage-conduit`: the MirageOS compatible implementation"""
  maintainer: "anil@recoil.org"
  authors: [
    "Anil Madhavapeddy" "Thomas Leonard" "Thomas Gazagnaire" "Rudi
  Grinberg"
  ]
  license: "ISC"
  tags: "org:mirage"
  homepage: "https://github.com/mirage/ocaml-conduit"
  doc: "https://mirage.github.io/ocaml-conduit/"
  bug-reports: "https://github.com/mirage/ocaml-conduit/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune" {build}
    "ppx_sexp_conv" {< "v0.13"}
    "sexplib" {< "v0.13"}
    "astring"
    "uri"
    "result"
    "logs" {>= "0.5.0"}
    "ipaddr" {>= "3.0.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-conduit.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-conduit/releases/download/v1.4.0/conduit-v1.4.0.tbz"
    checksum: [
      "md5=204222b8a61692083b79c67c8967fb28"
     
  "sha256=8de1e44effbc252fbf5862ba874a5608b8b99552e978762008be2625faf51206"
     
  "sha512=44a9d10cdc286135ec5096bfd43c65bb3a80758291cc804211494616725069bfed1c3d4a79b1c07262b048e2909786e2cf39b0393f1388d835ab10ea5267fbf8"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, ppx_sexp_conv, sexplib, astring, uri, ocaml-result,
  logs, ipaddr, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.4.0"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare ppx_sexp_conv "v0.13") < 0;
assert (vcompare sexplib "v0.13") < 0;
assert (vcompare logs "0.5.0") >= 0;
assert (vcompare ipaddr "3.0.0") >= 0;

stdenv.mkDerivation rec {
  pname = "conduit";
  version = "1.4.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-conduit/releases/download/v1.4.0/conduit-v1.4.0.tbz";
    sha256 = "01hjypx2a9my10h7cy79aaavkf08ar58gfk2b2zjy9dwzx7f9qcd";
  };
  buildInputs = [
    ocaml dune ppx_sexp_conv sexplib astring uri ocaml-result logs ipaddr
    findlib ];
  propagatedBuildInputs = [
    ocaml ppx_sexp_conv sexplib astring uri ocaml-result logs ipaddr ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
