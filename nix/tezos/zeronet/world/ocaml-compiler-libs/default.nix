/*opam-version: "2.0"
  name: "ocaml-compiler-libs"
  version: "v0.12.0"
  synopsis: "OCaml compiler libraries repackaged"
  description: """
  This packages exposes the OCaml compiler libraries repackages under
  the toplevel names Ocaml_common, Ocaml_bytecomp, ..."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "Apache-2.0"
  homepage: "https://github.com/janestreet/ocaml-compiler-libs"
  bug-reports:
  "https://github.com/janestreet/ocaml-compiler-libs/issues"
  depends: [
    "ocaml" {>= "4.04.1"}
    "dune" {build & >= "1.0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/ocaml-compiler-libs.git"
  url {
    src:
     
  "https://github.com/janestreet/ocaml-compiler-libs/archive/v0.12.0.tar.gz"
    checksum: [
      "md5=3351925ed99be59829641d2044fc80c0"
     
  "sha256=67caf5d96d8d2932341199dd365290aa31530f3c01d8e0f76e96535986aae294"
     
  "sha512=777054b3c943ea1176e7517ceb2080d1877c331a5f73cf42090d89dfcd59846b5f053ba1b511d61f2d7825073e0733dd41d222181797d4761019ac600c959127"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.12.0"; in
assert (vcompare ocaml "4.04.1") >= 0;
assert (vcompare dune "1.0") >= 0;

stdenv.mkDerivation rec {
  pname = "ocaml-compiler-libs";
  version = "v0.12.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/janestreet/ocaml-compiler-libs/archive/v0.12.0.tar.gz";
    sha256 = "1572ma35jlwndvvy1n017h7m6cdaj193dpcr24s34acddpczbjk7";
  };
  buildInputs = [
    ocaml dune findlib ];
  propagatedBuildInputs = [
    ocaml dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
