/*opam-version: "2.0"
  name: "lwt"
  version: "4.2.1"
  synopsis: "Promises and event-driven I/O"
  description: """
  A promise is a value that may become determined in the future.
  
  Lwt provides typed, composable promises. Promises that are resolved by I/O
  are
  resolved by Lwt in parallel.
  
  Meanwhile, OCaml code, including code creating and waiting on promises,
  runs in
  a single thread by default. This reduces the need for locks or
  other
  synchronization primitives. Code can be run in parallel on an opt-in
  basis."""
  maintainer: [
    "Anton Bachin <antonbachin@yahoo.com>"
    "Mauricio Fernandez <mfp@acm.org>"
    "Simon Cruanes <simon.cruanes.2007@m4x.org>"
  ]
  authors: ["Jérôme Vouillon" "Jérémie Dimino"]
  license: "MIT"
  homepage: "https://github.com/ocsigen/lwt"
  doc: "https://ocsigen.org/lwt/manual/"
  bug-reports: "https://github.com/ocsigen/lwt/issues"
  depends: [
    "cppo" {build & >= "1.1.0"}
    "dune"
    "mmap"
    "ocaml" {>= "4.02.0"}
    "result"
    "seq"
    "bisect_ppx" {dev & >= "1.3.0"}
    "ocamlfind" {dev & >= "1.7.3-1"}
  ]
  depopts: ["base-threads" "base-unix" "conf-libev"]
  conflicts: [
    "ocaml-variants" {= "4.02.1+BER"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/ocsigen/lwt.git"
  url {
    src: "https://github.com/ocsigen/lwt/archive/4.2.1.tar.gz"
    checksum: [
      "md5=9d648386ca0a9978eb9487de36b781cc"
     
  "sha256=6663be42156c7224c4d2c57842e669d99895dbeb87ebeb46deb90c5a0f0830c1"
     
  "sha512=4289568e9ca0ffc8625caddc0b5c7d79737800106289931927133eeeb79e3c638aad4399b075bf46b339e91a739589983ce546f4dad07f492fd31ce5be5c216d"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, cppo, dune, mmap, ocaml, ocaml-result, seq, bisect_ppx ? null,
  findlib, base-threads ? null, base-unix ? null, conf-libev ? null, ncurses
  }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "4.2.1"; in
assert (vcompare cppo "1.1.0") >= 0;
assert (vcompare ocaml "4.02.0") >= 0;
assert buildAsDev -> (vcompare bisect_ppx "1.3.0") >= 0;
assert buildAsDev -> (vcompare findlib "1.7.3-1") >= 0;

stdenv.mkDerivation rec {
  pname = "lwt";
  version = "4.2.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocsigen/lwt/archive/4.2.1.tar.gz";
    sha256 = "1h9h107ml35rvr3fpsw7xgdrb66rd7k44y65sb228wkc2m1bwqv6";
  };
  buildInputs = [
    cppo dune mmap ocaml ocaml-result seq bisect_ppx findlib ]
  ++
  stdenv.lib.optional
  (base-threads
  !=
  null)
  base-threads
  ++
  stdenv.lib.optional
  (base-unix
  !=
  null)
  base-unix
  ++
  stdenv.lib.optional
  (conf-libev
  !=
  null)
  conf-libev
  ++
  [
    ncurses ];
  propagatedBuildInputs = [
    cppo dune mmap ocaml ocaml-result seq bisect_ppx findlib ]
  ++
  stdenv.lib.optional
  (base-threads
  !=
  null)
  base-threads
  ++
  stdenv.lib.optional
  (base-unix
  !=
  null)
  base-unix
  ++
  stdenv.lib.optional
  (conf-libev
  !=
  null)
  conf-libev
  ++
  [
    ncurses ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
