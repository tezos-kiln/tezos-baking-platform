/*opam-version: "2.0"
  name: "sexplib0"
  version: "v0.12.0"
  synopsis:
    "Library containing the definition of S-expressions and some base
  converters"
  description: """
  Part of Jane Street's Core library
  The Core suite of libraries is an industrial strength alternative to
  OCaml's standard library that was developed by Jane Street, the
  largest industrial user of OCaml."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/sexplib0"
  doc:
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/sexplib0/index.html"
  bug-reports: "https://github.com/janestreet/sexplib0/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "dune" {>= "1.5.1"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/sexplib0.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.12/files/sexplib0-v0.12.0.tar.gz"
    checksum: [
      "md5=2486a25d3a94da9a94acc018b5f09061"
     
  "sha256=00d2f350ec78318fca04f097160054bacbaf0a7183f52eb9e240900fe117e0b3"
     
  "sha512=104f8526a147cfa6ecf168c8ee10a78ad3133c0a8a3714dc9dffd6358152752b337262b857c89170e07071ae669724da6f5556304c5abb3e9ce505ec700b7ad8"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.12.0"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare dune "1.5.1") >= 0;

stdenv.mkDerivation rec {
  pname = "sexplib0";
  version = "v0.12.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.12/files/sexplib0-v0.12.0.tar.gz";
    sha256 = "1cz02zhhz420wawjxxc3f45azjxsah01d5zh0k58ycbqxi8g7lh0";
  };
  buildInputs = [
    ocaml dune findlib ];
  propagatedBuildInputs = [
    ocaml dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
