/*opam-version: "2.0"
  name: "conduit-lwt-unix"
  version: "2.0.1"
  synopsis: "A network connection establishment library for
  Lwt_unix"
  maintainer: "anil@recoil.org"
  authors: [
    "Anil Madhavapeddy" "Thomas Leonard" "Thomas Gazagnaire" "Rudi
  Grinberg"
  ]
  license: "ISC"
  tags: "org:mirage"
  homepage: "https://github.com/mirage/ocaml-conduit"
  bug-reports: "https://github.com/mirage/ocaml-conduit/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune"
    "base-unix"
    "ppx_sexp_conv" {>= "v0.9.0"}
    "conduit-lwt" {= version}
    "lwt" {>= "3.0.0"}
    "uri" {>= "1.9.4"}
    "ipaddr" {>= "4.0.0"}
    "ipaddr-sexp"
  ]
  depopts: ["tls" "lwt_ssl" "launchd"]
  conflicts: [
    "tls" {< "0.8.0"}
    "ssl" {< "0.5.9"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-conduit.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-conduit/releases/download/v2.0.1/conduit-v2.0.1.tbz"
    checksum: [
     
  "sha256=faf9c1a74bb9f7e0c97637a96968c5198a9344b1dfccbbc2d124d74ac3bedfbb"
     
  "sha512=af30cb72ca65e619eb3f38ab3633c1f0ab28dbd7eedd10bcb80f449db9c9b7c433b8553adcb05ac1590ece1797e55bbe5e915255b1ad2fa2dff461a2bfc488aa"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, base-unix, ppx_sexp_conv, conduit-lwt, lwt, uri,
  ipaddr, ipaddr-sexp, findlib, tls ? null, lwt_ssl ? null, launchd ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "2.0.1"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare ppx_sexp_conv "v0.9.0") >= 0;
assert stdenv.lib.getVersion conduit-lwt == version;
assert (vcompare lwt "3.0.0") >= 0;
assert (vcompare uri "1.9.4") >= 0;
assert (vcompare ipaddr "4.0.0") >= 0;
assert tls != null -> !((vcompare tls "0.8.0") < 0);

stdenv.mkDerivation rec {
  pname = "conduit-lwt-unix";
  version = "2.0.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-conduit/releases/download/v2.0.1/conduit-v2.0.1.tbz";
    sha256 = "1fyzpv1lmmr4s71bpk6zn52972hrqml6ka9pfv4y1xxr9fkw3ygs";
  };
  buildInputs = [
    ocaml dune base-unix ppx_sexp_conv conduit-lwt lwt uri ipaddr ipaddr-sexp
    findlib ]
  ++
  stdenv.lib.optional
  (tls
  !=
  null)
  tls
  ++
  stdenv.lib.optional
  (lwt_ssl
  !=
  null)
  lwt_ssl
  ++
  stdenv.lib.optional
  (launchd
  !=
  null)
  launchd;
  propagatedBuildInputs = [
    ocaml dune base-unix ppx_sexp_conv conduit-lwt lwt uri ipaddr ipaddr-sexp ]
  ++
  stdenv.lib.optional
  (tls
  !=
  null)
  tls
  ++
  stdenv.lib.optional
  (lwt_ssl
  !=
  null)
  lwt_ssl
  ++
  stdenv.lib.optional
  (launchd
  !=
  null)
  launchd;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
