/*opam-version: "2.0"
  name: "uri-sexp"
  version: "3.0.0"
  synopsis: "An RFC3986 URI/URL parsing library"
  description: "ocaml-uri with sexp support"
  maintainer: "anil@recoil.org"
  authors: ["Anil Madhavapeddy" "David Sheets" "Rudi Grinberg"]
  license: "ISC"
  tags: ["url" "uri" "org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-uri"
  doc: "https://mirage.github.io/ocaml-uri/"
  bug-reports: "https://github.com/mirage/ocaml-uri/issues"
  depends: [
    "ocaml"
    "uri" {= version}
    "dune" {>= "1.2.0"}
    "ppx_sexp_conv" {>= "v0.9.0"}
    "sexplib0"
    "ounit" {with-test}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-uri.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-uri/releases/download/v3.0.0/uri-v3.0.0.tbz"
    checksum: [
     
  "sha256=8fb334fba6ebbf879e2e82d80d6adee8bdaf6cec3bb3da248110d805477d19fa"
     
  "sha512=553c18032a7c96cccdc8e37f497ce34e821b9dd089cfc8685783b7ade1d4dfa422722e4724abcba8b1171b51fa91a2bee297396fc7c349118069b6352e07881e"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, uri, dune, ppx_sexp_conv, sexplib0, ounit ? null, findlib
  }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "3.0.0"; in
assert stdenv.lib.getVersion uri == version;
assert (vcompare dune "1.2.0") >= 0;
assert (vcompare ppx_sexp_conv "v0.9.0") >= 0;

stdenv.mkDerivation rec {
  pname = "uri-sexp";
  version = "3.0.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-uri/releases/download/v3.0.0/uri-v3.0.0.tbz";
    sha256 = "1yhrgm3hbn0hh4jdmcrvxinazgg8vrm0vn425sg8ggzblvxk9cwg";
  };
  buildInputs = [
    ocaml uri dune ppx_sexp_conv sexplib0 ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    findlib ];
  propagatedBuildInputs = [
    ocaml uri dune ppx_sexp_conv sexplib0 ]
  ++
  stdenv.lib.optional
  doCheck
  ounit;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
