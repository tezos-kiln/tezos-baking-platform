/*opam-version: "2.0"
  name: "stdio"
  version: "v0.12.0"
  synopsis: "Standard IO library for OCaml"
  description: """
  Stdio implements simple input/output functionalities for OCaml.
  
  It re-exports the input/output functions of the OCaml standard
  libraries using a more consistent API."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/stdio"
  doc:
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/stdio/index.html"
  bug-reports: "https://github.com/janestreet/stdio/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "base" {>= "v0.12" & < "v0.13"}
    "dune" {>= "1.5.1"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/stdio.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.12/files/stdio-v0.12.0.tar.gz"
    checksum: [
      "md5=b261ff2d5667fde960c95e50cff668da"
     
  "sha256=4039873a52e061ec27240a6f9cf1869ff85e070bcbf8955f5779a88f5fc9bb85"
     
  "sha512=53a8cef84964a2c8c47d5f0572897079cbc5c4983a923f069c6d0ca4558d1c87662eb13b630501815bc2448772312b93735c70813638a786f5213a09b63e03e8"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.12.0"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare base "v0.12") >= 0 && (vcompare base "v0.13") < 0;
assert (vcompare dune "1.5.1") >= 0;

stdenv.mkDerivation rec {
  pname = "stdio";
  version = "v0.12.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.12/files/stdio-v0.12.0.tar.gz";
    sha256 = "11dvr5gqza3raxgrby6b1c3mxy4zhvqrqvqa4hkyqqg0a8x8ffa0";
  };
  buildInputs = [
    ocaml base dune findlib ];
  propagatedBuildInputs = [
    ocaml base dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
