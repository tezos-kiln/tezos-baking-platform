/*opam-version: "2.0"
  name: "digestif"
  version: "0.8.0"
  synopsis: "Hashes implementations (SHA*, RIPEMD160, BLAKE2* and
  MD5)"
  description: """
  Digestif is a toolbox to provide hashes implementations in C and OCaml.
  
  It uses the linking trick and user can decide at the end to use the C
  implementation or the OCaml implementation.
  
  We provides implementation of:
   * MD5
   * SHA1
   * SHA224
   * SHA256
   * SHA384
   * SHA512
   * BLAKE2B
   * BLAKE2S
   * RIPEMD160"""
  maintainer: [
    "Eyyüb Sari <eyyub.sari@epitech.eu>"
    "Romain Calascibetta <romain.calascibetta@gmail.com>"
  ]
  authors: [
    "Eyyüb Sari <eyyub.sari@epitech.eu>"
    "Romain Calascibetta <romain.calascibetta@gmail.com>"
  ]
  license: "MIT"
  homepage: "https://github.com/mirage/digestif"
  doc: "https://mirage.github.io/digestif/"
  bug-reports: "https://github.com/mirage/digestif/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune" {>= "1.9.2"}
    "eqaf"
    "base-bytes"
    "bigarray-compat"
    "stdlib-shims"
    "fmt" {with-test}
    "alcotest" {with-test}
  ]
  depopts: ["ocaml-freestanding" "mirage-xen-posix"]
  conflicts: [
    "mirage-xen-posix" {< "3.1.0"}
    "ocaml-freestanding" {< "0.4.3"}
    "mirage-runtime" {< "4.0.0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  run-test: ["dune" "runtest" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/mirage/digestif.git"
  url {
    src:
     
  "https://github.com/mirage/digestif/releases/download/v0.8.0/digestif-v0.8.0.tbz"
    checksum: [
     
  "sha256=9e35599404f1cbbb74beeaefc38e1fbf333e0ca84437b4256527f1889ffde425"
     
  "sha512=ce2ebccaae85c15c26781dc7101403e0e09cfe01d76bf9c5e298bd408257c5cc2c06d46481dfe3cfa87ec98e8065874a213960582b4e0e218447c851fc175ec4"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, eqaf, base-bytes, bigarray-compat, stdlib-shims,
  fmt ? null, alcotest ? null, findlib, ocaml-freestanding ? null,
  mirage-xen-posix ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.8.0"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare dune "1.9.2") >= 0;
assert mirage-xen-posix != null -> !((vcompare mirage-xen-posix "3.1.0") <
  0);
assert ocaml-freestanding != null -> !((vcompare ocaml-freestanding "0.4.3")
  < 0);

stdenv.mkDerivation rec {
  pname = "digestif";
  version = "0.8.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/digestif/releases/download/v0.8.0/digestif-v0.8.0.tbz";
    sha256 = "09g4zngqiw97cljv8ds4m063wcxz3y7c7vzaprsbpjzi0ja5jdcy";
  };
  buildInputs = [
    ocaml dune eqaf base-bytes bigarray-compat stdlib-shims ]
  ++
  stdenv.lib.optional
  doCheck
  fmt
  ++
  stdenv.lib.optional
  doCheck
  alcotest
  ++
  [
    findlib ]
  ++
  stdenv.lib.optional
  (ocaml-freestanding
  !=
  null)
  ocaml-freestanding
  ++
  stdenv.lib.optional
  (mirage-xen-posix
  !=
  null)
  mirage-xen-posix;
  propagatedBuildInputs = [
    ocaml dune eqaf base-bytes bigarray-compat stdlib-shims ]
  ++
  stdenv.lib.optional
  doCheck
  fmt
  ++
  stdenv.lib.optional
  doCheck
  alcotest
  ++
  stdenv.lib.optional
  (ocaml-freestanding
  !=
  null)
  ocaml-freestanding
  ++
  stdenv.lib.optional
  (mirage-xen-posix
  !=
  null)
  mirage-xen-posix;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
