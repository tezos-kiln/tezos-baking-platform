/*opam-version: "2.0"
  name: "sexplib"
  version: "v0.12.0"
  synopsis: "Library for serializing OCaml values to and from
  S-expressions"
  description: """
  Part of Jane Street's Core library
  The Core suite of libraries is an industrial strength alternative to
  OCaml's standard library that was developed by Jane Street, the
  largest industrial user of OCaml."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/sexplib"
  doc:
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/sexplib/index.html"
  bug-reports: "https://github.com/janestreet/sexplib/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "parsexp" {>= "v0.12" & < "v0.13"}
    "sexplib0" {>= "v0.12" & < "v0.13"}
    "dune" {>= "1.5.1"}
    "num"
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/sexplib.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.12/files/sexplib-v0.12.0.tar.gz"
    checksum: [
      "md5=a7f9f8a414aed6cc56901199cda020f6"
     
  "sha256=c54b11b7d30eb4d6587834f9011b37f94619b76cdc496ea22633079ced59827f"
     
  "sha512=bd050e59f5269f15b3362891f98417c78bbe6e18c630488ac3df769dd70180beb4e1bbf55e32327fd2dec9a6041969bcaa4f9d16b9295e33cc82af1515404701"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, parsexp, sexplib0, dune, num, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.12.0"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare parsexp "v0.12") >= 0 && (vcompare parsexp "v0.13") < 0;
assert (vcompare sexplib0 "v0.12") >= 0 && (vcompare sexplib0 "v0.13") < 0;
assert (vcompare dune "1.5.1") >= 0;

stdenv.mkDerivation rec {
  pname = "sexplib";
  version = "v0.12.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.12/files/sexplib-v0.12.0.tar.gz";
    sha256 = "0zw2b7nrq1rk4si6wjfwdjvijipr6wdh3y9lg1cddd0fsfvi2jy5";
  };
  buildInputs = [
    ocaml parsexp sexplib0 dune num findlib ];
  propagatedBuildInputs = [
    ocaml parsexp sexplib0 dune num ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
