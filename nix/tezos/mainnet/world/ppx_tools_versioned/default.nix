/*opam-version: "2.0"
  name: "ppx_tools_versioned"
  version: "5.2.2"
  synopsis: "A variant of ppx_tools based on
  ocaml-migrate-parsetree"
  maintainer: "frederic.bour@lakaban.net"
  authors: [
    "Frédéric Bour <frederic.bour@lakaban.net>"
    "Alain Frisch <alain.frisch@lexifi.com>"
  ]
  license: "MIT"
  tags: "syntax"
  homepage: "https://github.com/ocaml-ppx/ppx_tools_versioned"
  bug-reports:
  "https://github.com/ocaml-ppx/ppx_tools_versioned/issues"
  depends: [
    "ocaml" {>= "4.02.0"}
    "dune" {>= "1.0"}
    "ocaml-migrate-parsetree" {>= "1.0.10"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git://github.com/ocaml-ppx/ppx_tools_versioned.git"
  url {
    src:
      "https://github.com/ocaml-ppx/ppx_tools_versioned/archive/5.2.2.tar.gz"
    checksum: [
      "md5=f78a3c2b4cc3b92702e1f7096a6125fa"
     
  "sha512=68c168ebc01af46fe8766ad7e36cc778caabb97d8eb303db284d106450cb79974c2a640ce459e197630b9e84b02caa24b59c97c9a8d39ddadc7efc7284e42a70"
     
  "sha256=d0310556d77925a992f69eaebeeba94e0178de729b75972319e496f58557a828"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, ocaml-migrate-parsetree, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "5.2.2"; in
assert (vcompare ocaml "4.02.0") >= 0;
assert (vcompare dune "1.0") >= 0;
assert (vcompare ocaml-migrate-parsetree "1.0.10") >= 0;

stdenv.mkDerivation rec {
  pname = "ppx_tools_versioned";
  version = "5.2.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-ppx/ppx_tools_versioned/archive/5.2.2.tar.gz";
    sha256 = "0a58ay2zb5p434irfxcvfbg7h0afm7mvxblyys9aj9brsxb0acfh";
  };
  buildInputs = [
    ocaml dune ocaml-migrate-parsetree findlib ];
  propagatedBuildInputs = [
    ocaml dune ocaml-migrate-parsetree ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
