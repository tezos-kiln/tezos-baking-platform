/*opam-version: "2.0"
  name: "ipaddr"
  version: "4.0.0"
  synopsis:
    "A library for manipulation of IP (and MAC) address
  representations"
  description: """
  Features:
   * Depends only on sexplib (conditionalization under consideration)
   * oUnit-based tests
   * IPv4 and IPv6 support
   * IPv4 and IPv6 CIDR prefix support
   * IPv4 and IPv6 [CIDR-scoped
  address](http://tools.ietf.org/html/rfc4291#section-2.3) support
   * `Ipaddr.V4` and `Ipaddr.V4.Prefix` modules are `Map.OrderedType`
   * `Ipaddr.V6` and `Ipaddr.V6.Prefix` modules are `Map.OrderedType`
   * `Ipaddr` and `Ipaddr.Prefix` modules are `Map.OrderedType`
   * `Ipaddr_unix` in findlib subpackage `ipaddr.unix` provides compatibility
  with the standard library `Unix` module
   * `Ipaddr_top` in findlib subpackage `ipaddr.top` provides top-level
  pretty printers (requires compiler-libs default since OCaml 4.0)
   * IP address scope classification
   * IPv4-mapped addresses in IPv6 (::ffff:0:0/96) are an embedding of IPv4
   * MAC-48 (Ethernet) address support
   * `Macaddr` is a `Map.OrderedType`
   * All types have sexplib serializers/deserializers"""
  maintainer: "anil@recoil.org"
  authors: ["David Sheets" "Anil Madhavapeddy" "Hugo Heuzard"]
  license: "ISC"
  tags: ["org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-ipaddr"
  doc: "https://mirage.github.io/ocaml-ipaddr/"
  bug-reports: "https://github.com/mirage/ocaml-ipaddr/issues"
  depends: [
    "ocaml" {>= "4.04.0"}
    "dune" {>= "1.9.0"}
    "macaddr" {= version}
    "sexplib0" {< "v0.13"}
    "domain-name" {>= "0.3.0"}
    "ounit" {with-test}
    "ppx_sexp_conv" {with-test & >= "v0.9.0" & < "v0.13"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-ipaddr.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-ipaddr/releases/download/v4.0.0/ipaddr-v4.0.0.tbz"
    checksum: [
     
  "sha256=6f4abf9c210b20ccddf4610691a87b8c870790d8f71d4a7edcfca9e21b59fc29"
     
  "sha512=ca55a8cfa8b84c0a2f4e1fe7afb4c582066bbb562efb94169c0347e441ce076dc426d191772edb869eca6bd77f42f7141378181057ad8886da25ef915a9ee57f"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, macaddr, sexplib0, domain-name, ounit ? null,
  ppx_sexp_conv ? null, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "4.0.0"; in
assert (vcompare ocaml "4.04.0") >= 0;
assert (vcompare dune "1.9.0") >= 0;
assert stdenv.lib.getVersion macaddr == version;
assert (vcompare sexplib0 "v0.13") < 0;
assert (vcompare domain-name "0.3.0") >= 0;
assert doCheck -> (vcompare ppx_sexp_conv "v0.9.0") >= 0 && (vcompare
  ppx_sexp_conv "v0.13") < 0;

stdenv.mkDerivation rec {
  pname = "ipaddr";
  version = "4.0.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-ipaddr/releases/download/v4.0.0/ipaddr-v4.0.0.tbz";
    sha256 = "0agwb4dy5agwviz4l7gpv280g1wcgfl921k1ykfwq80b46fbyjkg";
  };
  buildInputs = [
    ocaml dune macaddr sexplib0 domain-name ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    ppx_sexp_conv findlib ];
  propagatedBuildInputs = [
    ocaml dune macaddr sexplib0 domain-name ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    ppx_sexp_conv ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
