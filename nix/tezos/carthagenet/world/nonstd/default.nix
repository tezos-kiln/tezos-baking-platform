/*opam-version: "2.0"
  name: "nonstd"
  version: "0.0.3"
  synopsis: "Non-standard mini-library"
  description: """
  Core-style (labels, exceptionless) pure-OCaml super-light library
  providing basic modules: List, Option, Int. and Float."""
  maintainer: "seb@mondet.org"
  authors: [
    "Sebastien Mondet <seb@modnet.org>" "Leonid Rozenberg
  <leonidr@gmail.com>"
  ]
  homepage: "https://bitbucket.org/smondet/nonstd"
  bug-reports: "https://bitbucket.org/smondet/nonstd"
  depends: [
    "ocaml" {>= "4.02.0"}
    "ocamlfind"
    "jbuilder"
  ]
  build: [
    "jbuilder" "build" "--only" "nonstd" "--root" "." "-j" jobs
  "@install"
  ]
  dev-repo: "git+https://bitbucket.org/smondet/nonstd.git"
  url {
    src: "https://bitbucket.org/smondet/nonstd/get/nonstd.0.0.3.tar.gz"
    checksum: [
      "md5=784401ed67aa323e03544e0f801abefd"
     
  "sha256=46ac91043bebd7ac098f377e83f233c8b833e3f52728ab3162d652dcb8bac9be"
     
  "sha512=f85eb6a5c57df0d3ddca6ba7c9f5931db5f50ed49c7796ff15b0c8ea4cde23152c5e47ff121d0d2e3bf32905087192ccad896d08a77921ae851c58e3332f3970"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, jbuilder }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.0.3"; in
assert (vcompare ocaml "4.02.0") >= 0;

stdenv.mkDerivation rec {
  pname = "nonstd";
  version = "0.0.3";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://bitbucket.org/smondet/nonstd/get/nonstd.0.0.3.tar.gz";
    sha256 = "1gn9pawdqlnnc8qsna17ypik7f686gr86zipiw4srmzb7c293b26";
  };
  buildInputs = [
    ocaml findlib jbuilder ];
  propagatedBuildInputs = [
    ocaml findlib jbuilder ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "'jbuilder'" "'build'" "'--only'" "'nonstd'" "'--root'" "'.'" "'-j'"
      "1" "'@install'" ]
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
