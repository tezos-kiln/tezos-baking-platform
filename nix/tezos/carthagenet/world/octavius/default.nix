/*opam-version: "2.0"
  name: "octavius"
  version: "1.2.1"
  synopsis: "Ocamldoc comment syntax parser"
  description: "Octavius is a library to parse the `ocamldoc` comment
  syntax."
  maintainer: "leo@lpw25.net"
  authors: "Leo White <leo@lpw25.net>"
  license: "ISC"
  tags: ["doc" "ocamldoc" "org:ocaml-doc"]
  homepage: "https://github.com/ocaml-doc/octavius"
  doc: "http://ocaml-doc.github.io/octavius/"
  bug-reports: "https://github.com/ocaml-doc/octavius/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "jbuilder" {>= "1.0+beta7"}
  ]
  build: [
    ["jbuilder" "subst" "-p" name] {pinned}
    ["jbuilder" "build" "-p" name "-j" jobs]
  ]
  dev-repo: "git+http://github.com/ocaml-doc/octavius.git"
  url {
    src: "https://github.com/ocaml-doc/octavius/archive/v1.2.1.tar.gz"
    checksum: [
      "md5=fe5f2e1ea8eba9f8c618580a34942bf1"
     
  "sha256=527ce08eda6df9ed71f143b384ec0d330084279554862ab9c600a1cda5cce1fe"
     
  "sha512=4d0c0206312cc5272d459f8b73467074724450a67eb2e8a00129fadeccc5ddec69efcb3cb60f7d510f614c01142bd6b4dbec845f1da452f810f5ab28db93fa94"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, jbuilder, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.2.1"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare jbuilder "1.0+beta7") >= 0;

stdenv.mkDerivation rec {
  pname = "octavius";
  version = "1.2.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-doc/octavius/archive/v1.2.1.tar.gz";
    sha256 = "1zp1rjjwv880qswjm1jljlkq801k1pn89cs3y5qyvybdva7f0z2j";
  };
  buildInputs = [
    ocaml jbuilder findlib ];
  propagatedBuildInputs = [
    ocaml jbuilder ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'jbuilder'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
