/*opam-version: "2.0"
  name: "opam-depext"
  version: "1.1.3"
  synopsis: "Query and install external dependencies of OPAM
  packages"
  description: """
  opam-depext is a simple program intended to facilitate the interaction
  between
  OPAM packages and the host package management system. It can query OPAM for
  the
  right external dependencies on a set of packages, depending on the host OS,
  and
  call the OS's package manager in the appropriate way to install
  them."""
  maintainer: [
    "Louis Gesbert <louis.gesbert@ocamlpro.com>"
    "Anil Madhavapeddy <anil@recoil.org>"
  ]
  authors: [
    "Louis Gesbert <louis.gesbert@ocamlpro.com>"
    "Anil Madhavapeddy <anil@recoil.org>"
  ]
  license: "LGPL-2.1 with OCaml linking exception"
  homepage: "https://github.com/ocaml/opam-depext"
  bug-reports: "https://github.com/ocaml/opam-depext/issues"
  depends: [
    "ocaml" {>= "4.0.0"}
  ]
  available: opam-version >= "2.0.0~beta5"
  flags: plugin
  build: make
  dev-repo: "git+https://github.com/ocaml/opam-depext.git#2.0"
  url {
    src:
     
  "https://github.com/ocaml/opam-depext/releases/download/v1.1.3/opam-depext-full-1.1.3.tbz"
    checksum: [
      "md5=72c082a0a6606398669b6fc2f4c4ae84"
     
  "sha256=1ff6144702b315f29c1ed3dfc8c1f5cc231f66558151e146e12e08bce43b27d7"
     
  "sha512=e901b2c79af74dadf83f3a64958881628da49ab22a3bceef938152b8426ab9f6b230cb01eb302f2fbe97b0aae65feca8f482bfc555d59acd6f0f17d45f27976c"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.1.3"; in
assert (vcompare ocaml "4.0.0") >= 0;

stdenv.mkDerivation rec {
  pname = "opam-depext";
  version = "1.1.3";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml/opam-depext/releases/download/v1.1.3/opam-depext-full-1.1.3.tbz";
    sha256 = "1mr77gjbq21fw53f2lc1amk1y8ycyp0wipyk3sfg45dk093i9xhz";
  };
  buildInputs = [
    ocaml findlib ];
  propagatedBuildInputs = [
    ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
