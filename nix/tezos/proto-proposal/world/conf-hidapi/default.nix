/*opam-version: "2.0"
  name: "conf-hidapi"
  version: "0"
  synopsis: "Virtual package relying on a hidapi system
  installation"
  description:
    "This package can only install if the hidapi lib is installed on the
  system."
  maintainer: "Vincent Bernardoff"
  authors: "Signal 11 Software"
  license: "BSD"
  homepage: "http://www.signal11.us/oss/hidapi/"
  bug-reports: "https://github.com/ocaml/opam-repository/issues"
  depends: [
    "conf-pkg-config" {build}
  ]
  flags: conf
  build: [
    ["pkg-config" "hidapi-libusb"] {os != "macos" & os != "freebsd"}
    ["pkg-config" "hidapi"] {os = "macos" | os = "freebsd"}
  ]
  depexts: [
    ["libhidapi-dev"] {os-family = "debian"}
    ["hidapi"] {os-distribution = "arch"}
    ["hidapi"] {os = "macos" & os-distribution = "homebrew"}
    ["hidapi"] {os = "freebsd"}
    ["hidapi-dev"] {os-distribution = "alpine"}
    ["epel-release" "hidapi-devel"] {os-distribution = "centos"}
  ]*/
{ runCommand, doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv,
  opam, fetchurl, conf-pkg-config, findlib, hidapi }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0"; in

stdenv.mkDerivation rec {
  pname = "conf-hidapi";
  version = "0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = runCommand
  "empty"
  {
    outputHashMode = "recursive";
    outputHashAlgo = "sha256";
    outputHash = "0sjjj9z1dhilhpc8pq4154czrb79z9cm044jvn75kxcjv6v5l2m5";
  }
  "mkdir $out";
  buildInputs = [
    conf-pkg-config findlib hidapi ];
  propagatedBuildInputs = [
    hidapi ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'pkg-config'" "'hidapi-libusb'" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
