/*opam-version: "2.0"
  name: "ppx_fields_conv"
  version: "v0.13.0"
  synopsis: "Generation of accessor and iteration functions for ocaml
  records"
  description: "Part of the Jane Street's PPX rewriters
  collection."
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/ppx_fields_conv"
  doc:
   
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/ppx_fields_conv/index.html"
  bug-reports: "https://github.com/janestreet/ppx_fields_conv/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "base" {>= "v0.13" & < "v0.14"}
    "fieldslib" {>= "v0.13" & < "v0.14"}
    "dune" {>= "1.5.1"}
    "ppxlib" {>= "0.9.0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/ppx_fields_conv.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.13/files/ppx_fields_conv-v0.13.0.tar.gz"
    checksum: [
      "md5=02c84db8ce53d6da9316bacae27d8d15"
     
  "sha256=527fce283845613ca20d6460a1edfc3e91d084959661c1837a3b02a357dd490e"
     
  "sha512=cf9b0715dc4c0b7762ea4e0bf12983005cbbc11148a4753a20f3cd3d5d08d6cba546a35f31d215270475e877fe8d84491ed46e087e7b626c412282f9d5d7ac58"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, fieldslib, dune, ppxlib, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.13.0"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare base "v0.13") >= 0 && (vcompare base "v0.14") < 0;
assert (vcompare fieldslib "v0.13") >= 0 && (vcompare fieldslib "v0.14") < 0;
assert (vcompare dune "1.5.1") >= 0;
assert (vcompare ppxlib "0.9.0") >= 0;

stdenv.mkDerivation rec {
  pname = "ppx_fields_conv";
  version = "v0.13.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.13/files/ppx_fields_conv-v0.13.0.tar.gz";
    sha256 = "03j9vmbs60ivga1w2qcnjn2d149yzkns2q341ni3qqa570lcwzsj";
  };
  buildInputs = [
    ocaml base fieldslib dune ppxlib findlib ];
  propagatedBuildInputs = [
    ocaml base fieldslib dune ppxlib ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
