/*opam-version: "2.0"
  name: "lwt-watcher"
  version: "0.1"
  synopsis: "One-to-many broadcast in Lwt"
  maintainer: "contact@tezos.com"
  authors: "Tezos devteam"
  license: "MIT"
  homepage: "https://gitlab.com/nomadic-labs/lwt-watcher"
  bug-reports: "https://gitlab.com/nomadic-labs/lwt-watcher/issues"
  depends: ["ocaml" "dune" "lwt"]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://gitlab.com/nomadic-labs/lwt-watcher.git"
  url {
    src:
     
  "https://gitlab.com/nomadic-labs/lwt-watcher/-/archive/v0.1/lwt-watcher-v0.1.tar.gz"
    checksum: [
      "md5=85fa938c25895aad8f47872ed5225a4e"
     
  "sha512=7a93368ab6133e17fdf8512c28380fdba5bd0eb397deac9c7018278138b4fa4e4d65b5f32af624a0c725087640ae105be890d93d9019ca90352c36fb43183b14"
     
  "sha256=a500212661f4385e25fab54477299cb4df4593d447c08d9629ecb0d5670cb45a"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, lwt, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.1"; in

stdenv.mkDerivation rec {
  pname = "lwt-watcher";
  version = "0.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://gitlab.com/nomadic-labs/lwt-watcher/-/archive/v0.1/lwt-watcher-v0.1.tar.gz";
    sha256 = "0nml1ikxbc7c56b8vh27sj9lbpxlkhlpfi5mz8jmwf7lc4k22055";
  };
  buildInputs = [
    ocaml dune lwt findlib ];
  propagatedBuildInputs = [
    ocaml dune lwt ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
