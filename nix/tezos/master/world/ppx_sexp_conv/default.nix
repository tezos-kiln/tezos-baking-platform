/*opam-version: "2.0"
  name: "ppx_sexp_conv"
  version: "v0.13.0"
  synopsis: "[@@deriving] plugin to generate S-expression conversion
  functions"
  description: "Part of the Jane Street's PPX rewriters
  collection."
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/ppx_sexp_conv"
  doc:
   
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/ppx_sexp_conv/index.html"
  bug-reports: "https://github.com/janestreet/ppx_sexp_conv/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "base" {>= "v0.13" & < "v0.14"}
    "sexplib0" {>= "v0.13" & < "v0.14"}
    "dune" {>= "1.5.1"}
    "ppxlib" {>= "0.9.0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/ppx_sexp_conv.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.13/files/ppx_sexp_conv-v0.13.0.tar.gz"
    checksum: [
      "md5=acb33a38721f4a16f71add6afaa315da"
     
  "sha256=1691e0c3f744512d863545cd85720e2ab27a211d8d4c4bc7591659f90ff1f77a"
     
  "sha512=c507c75a7f9e505ca3fd7b9815f09933c1799f8eeb741ad4592a64a85b6c9c5853dc04c3fac10151f0bec66103958ca9486706b6807a6f054f296621ff60189f"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, sexplib0, dune, ppxlib, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.13.0"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare base "v0.13") >= 0 && (vcompare base "v0.14") < 0;
assert (vcompare sexplib0 "v0.13") >= 0 && (vcompare sexplib0 "v0.14") < 0;
assert (vcompare dune "1.5.1") >= 0;
assert (vcompare ppxlib "0.9.0") >= 0;

stdenv.mkDerivation rec {
  pname = "ppx_sexp_conv";
  version = "v0.13.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.13/files/ppx_sexp_conv-v0.13.0.tar.gz";
    sha256 = "0yppy47zjn8nb73lnk4d3lhpmcia1rr8bka56n32sla4yz1y148n";
  };
  buildInputs = [
    ocaml base sexplib0 dune ppxlib findlib ];
  propagatedBuildInputs = [
    ocaml base sexplib0 dune ppxlib ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
