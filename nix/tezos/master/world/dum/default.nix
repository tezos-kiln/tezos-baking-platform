/*opam-version: "2.0"
  name: "dum"
  version: "1.0.1"
  synopsis: "Inspect the runtime representation of arbitrary OCaml
  values"
  description: """
  Dum is a library for inspecting the runtime representation of
  arbitrary OCaml values. Shared or cyclic data are detected
  and labelled. This guarantees that printing would always
  terminate. This makes it possible to print values such as closures,
  objects or exceptions in depth and without risk."""
  maintainer: "martin@mjambon.com"
  authors: ["Martin Jambon" "Jean-Christophe Filliatre" "Richard W.M.
  Jones"]
  license: "LGPL-2.1-or-later with OCaml-LGPL-linking-exception"
  homepage: "https://github.com/mjambon/dum"
  bug-reports: "https://github.com/mjambon/dum/issues"
  depends: ["ocaml" "ocamlfind" "easy-format"]
  flags: light-uninstall
  build: make
  install: [make "install"]
  remove: ["ocamlfind" "remove" "dum"]
  dev-repo: "git+https://github.com/mjambon/dum.git"
  url {
    src: "https://github.com/mjambon/dum/archive/v1.0.1.tar.gz"
    checksum: [
      "md5=aa5bd1ea89d2f5881c18652545361a1c"
     
  "sha256=973166d4a371c704154fa8121af4bf4c1906976d74f04344fa43d10acb8c899a"
     
  "sha512=dbe01d2222c5b6ec053d81e3e29bd010f419d2844ee8f209ed4c1eeed8fca8c9ffffe3a1b870d0b2b34f0f5fe6a3b36e99283aaf9ea14704b1fa855dfa454933"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, easy-format }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.0.1"; in

stdenv.mkDerivation rec {
  pname = "dum";
  version = "1.0.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mjambon/dum/archive/v1.0.1.tar.gz";
    sha256 = "16l9ik5hmla3z9247w3ldnbhc6acpzs1l4m89wah9ivilga6cccp";
  };
  buildInputs = [
    ocaml findlib easy-format ];
  propagatedBuildInputs = [
    ocaml findlib easy-format ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" "'install'" ] ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
