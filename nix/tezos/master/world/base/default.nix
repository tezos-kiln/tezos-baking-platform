/*opam-version: "2.0"
  name: "base"
  version: "v0.13.0"
  synopsis: "Full standard library replacement for OCaml"
  description: """
  Full standard library replacement for OCaml
  
  Base is a complete and portable alternative to the OCaml standard
  library. It provides all standard functionalities one would expect
  from a language standard library. It uses consistent conventions
  across all of its module.
  
  Base aims to be usable in any context. As a result system dependent
  features such as I/O are not offered by Base. They are instead
  provided by companion libraries such as stdio:
  
    https://github.com/janestreet/stdio"""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/base"
  doc:
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/base/index.html"
  bug-reports: "https://github.com/janestreet/base/issues"
  depends: [
    "ocaml" {>= "4.04.2" & < "4.10.0"}
    "sexplib0" {>= "v0.13" & < "v0.14"}
    "dune" {>= "1.5.1"}
    "dune-configurator"
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/base.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.13/files/base-v0.13.0.tar.gz"
    checksum: [
      "md5=527289dbc2c7de748f965d3caae450cb"
     
  "sha256=b59f9a2e54f178eaa311efa4e33cd15f4e31856a24309ced764bd408c8db4e0f"
     
  "sha512=238cbc5f0b08430edd0a0478ae720e0463c34ec5f572513b527b7a6c5cd731ad12de847787910931df749b50438e7f1e7dc12357e9cee7f4c1d737d2105cb47f"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, sexplib0, dune, dune-configurator, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.13.0"; in
assert (vcompare ocaml "4.04.2") >= 0 && (vcompare ocaml "4.10.0") < 0;
assert (vcompare sexplib0 "v0.13") >= 0 && (vcompare sexplib0 "v0.14") < 0;
assert (vcompare dune "1.5.1") >= 0;

stdenv.mkDerivation rec {
  pname = "base";
  version = "v0.13.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.13/files/base-v0.13.0.tar.gz";
    sha256 = "03sfvg40im2bfvnrqc14da2k2kjzs4yf797g26iyly7iahp9m7xm";
  };
  buildInputs = [
    ocaml sexplib0 dune dune-configurator findlib ];
  propagatedBuildInputs = [
    ocaml sexplib0 dune dune-configurator ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
