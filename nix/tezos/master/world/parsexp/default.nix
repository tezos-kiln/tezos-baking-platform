/*opam-version: "2.0"
  name: "parsexp"
  version: "v0.13.0"
  synopsis: "S-expression parsing library"
  description: """
  This library provides generic parsers for parsing S-expressions from
  strings or other medium.
  
  The library is focused on performances but still provide full
  generic
  parsers that can be used with strings, bigstrings, lexing buffers,
  character streams or any other sources effortlessly.
  
  It provides three different class of parsers:
  - the normal parsers, producing [Sexp.t] or [Sexp.t list] values
  - the parsers with positions, building compact position sequences so
    that one can recover original positions in order to report properly
    located errors at little cost
  - the Concrete Syntax Tree parsers, produce values of type
    [Parsexp.Cst.t] which record the concrete layout of the s-expression
    syntax, including comments
  
  This library is portable and doesn't provide IO functions. To
  read
  s-expressions from files or other external sources, you should
  use
  parsexp_io."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/parsexp"
  doc:
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/parsexp/index.html"
  bug-reports: "https://github.com/janestreet/parsexp/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "base" {>= "v0.13" & < "v0.14"}
    "sexplib0" {>= "v0.13" & < "v0.14"}
    "dune" {>= "1.5.1"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/parsexp.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.13/files/parsexp-v0.13.0.tar.gz"
    checksum: [
      "md5=08d2f6eca6a1eda735bf030d2581da43"
     
  "sha256=d806f07f363a1838820dff01606964abb891ddacb9ae879024bcda61a4242454"
     
  "sha512=68270ea51eb75ee4e61d50d17446a9dcd7307c6d2fe76dc877d85ed315959c31684971dc944155371bcec5a8d11f95c38bf3d2c6868e3c57a3c087ca48191850"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, sexplib0, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.13.0"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare base "v0.13") >= 0 && (vcompare base "v0.14") < 0;
assert (vcompare sexplib0 "v0.13") >= 0 && (vcompare sexplib0 "v0.14") < 0;
assert (vcompare dune "1.5.1") >= 0;

stdenv.mkDerivation rec {
  pname = "parsexp";
  version = "v0.13.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.13/files/parsexp-v0.13.0.tar.gz";
    sha256 = "0m144jj63nmw4j88gbmrmkfr3f5bciln00gz1n13h61s6rzz01nq";
  };
  buildInputs = [
    ocaml base sexplib0 dune findlib ];
  propagatedBuildInputs = [
    ocaml base sexplib0 dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
