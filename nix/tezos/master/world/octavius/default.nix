/*opam-version: "2.0"
  name: "octavius"
  version: "1.2.2"
  synopsis: "Ocamldoc comment syntax parser"
  description: "Octavius is a library to parse the `ocamldoc` comment
  syntax."
  maintainer: "leo@lpw25.net"
  authors: "Leo White <leo@lpw25.net>"
  license: "ISC"
  homepage: "https://github.com/ocaml-doc/octavius"
  doc: "http://ocaml-doc.github.io/octavius/"
  bug-reports: "https://github.com/ocaml-doc/octavius/issues"
  depends: [
    "dune" {>= "1.11"}
    "ocaml" {>= "4.03.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    [
      "dune"
      "build"
      "-p"
      name
      "-j"
      jobs
      "@install"
      "@runtest" {with-test}
      "@doc" {with-doc}
    ]
  ]
  dev-repo: "git+https://github.com/ocaml-doc/octavius.git"
  url {
    src: "https://github.com/ocaml-doc/octavius/archive/v1.2.2.tar.gz"
    checksum: [
      "md5=72f9e1d996e6c5089fc513cc9218607b"
     
  "sha256=eac9104ce0316b69da9c44b9c477700fe0b52a888c89ce4bdf1d2b782a73e0ad"
     
  "sha512=57e40f0bd03599bf0b14d85b29bef2214f9b9393917c0a65acf60cf028fe028a730a804f3c0101fee1378cacd4feb7a4f2df3c8411ceeb6bb34d47f849b7a55e"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, dune, ocaml, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.2.2"; in
assert (vcompare dune "1.11") >= 0;
assert (vcompare ocaml "4.03.0") >= 0;

stdenv.mkDerivation rec {
  pname = "octavius";
  version = "1.2.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-doc/octavius/archive/v1.2.2.tar.gz";
    sha256 = "1bg0fcm7haqxvx5wx2cci0mbbq0gf1vw9fa4kkd6jsriw1611jga";
  };
  buildInputs = [
    dune ocaml findlib ];
  propagatedBuildInputs = [
    dune ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ([ "'dune'" "'build'" "'-p'" pname "'-j'" "1" "'@install'" ] ++
    stdenv.lib.optional doCheck "'@runtest'" ++ stdenv.lib.optional buildDocs
    "'@doc'") ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
