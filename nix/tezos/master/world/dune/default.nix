/*opam-version: "2.0"
  name: "dune"
  version: "1.11.4"
  synopsis: "Fast, portable and opinionated build system"
  description: """
  dune is a build system that was designed to simplify the release of
  Jane Street packages. It reads metadata from "dune" files following a
  very simple s-expression syntax.
  
  dune is fast, it has very low-overhead and support parallel builds on
  all platforms. It has no system dependencies, all you need to build
  dune and packages using dune is OCaml. You don't need or make or bash
  as long as the packages themselves don't use bash explicitly.
  
  dune supports multi-package development by simply dropping
  multiple
  repositories into the same directory.
  
  It also supports multi-context builds, such as building against
  several opam roots/switches simultaneously. This helps maintaining
  packages across several versions of OCaml and gives cross-compilation
  for free."""
  maintainer: "Jane Street Group, LLC <opensource@janestreet.com>"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/ocaml/dune"
  doc: "https://dune.readthedocs.io/"
  bug-reports: "https://github.com/ocaml/dune/issues"
  depends: [
    "ocaml" {>= "4.02"}
    "base-unix"
    "base-threads"
  ]
  conflicts: [
    "jbuilder" {!= "transition"}
    "odoc" {< "1.3.0"}
    "dune-release" {< "1.3.0"}
  ]
  build: [
    ["ocaml" "configure.ml" "--libdir" lib] {opam-version < "2"}
    ["ocaml" "bootstrap.ml"]
    ["./boot.exe" "--release" "--subst"] {pinned}
    ["./boot.exe" "--release" "-j" jobs]
  ]
  dev-repo: "git+https://github.com/ocaml/dune.git"
  url {
    src:
     
  "https://github.com/ocaml/dune/releases/download/1.11.4/dune-build-info-1.11.4.tbz"
    checksum: [
     
  "sha256=77cb5f483221b266ded2b85fc84173ae0089a25134a086be922e82c131456ce6"
     
  "sha512=02f00fd872aa49b832fc8c1e928409f23c79ddf84a53009a58875f222cca36fbb92c905e12c539caec9cbad723f195a8aa24218382dca35a903b3f52b11f06f2"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base-unix, base-threads, findlib, fauxpam }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.11.4"; in
assert (vcompare ocaml "4.02") >= 0;

stdenv.mkDerivation rec {
  pname = "dune";
  version = "1.11.4";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml/dune/releases/download/1.11.4/dune-build-info-1.11.4.tbz";
    sha256 = "1rkc8lqw30ifjaz8d81la6i8j05ffd0whpxqsbg6dci16945zjvp";
  };
  buildInputs = [
    ocaml base-unix base-threads findlib fauxpam ];
  propagatedBuildInputs = [
    ocaml base-unix base-threads fauxpam ];
  configurePhase = "true";
  patches = [
    ./dune-libdir.patch ];
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    (stdenv.lib.optionals ((vcompare "2.0.0" "2") < 0) [
      "'ocaml'" "'configure.ml'" "'--libdir'" "$OCAMLFIND_DESTDIR" ])
    [ "'ocaml'" "'bootstrap.ml'" ] [
      "'./boot.exe'" "'--release'" "'-j'" "1" ]
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
