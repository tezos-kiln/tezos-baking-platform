/*opam-version: "2.0"
  name: "sexplib"
  version: "v0.13.0"
  synopsis: "Library for serializing OCaml values to and from
  S-expressions"
  description: """
  Part of Jane Street's Core library
  The Core suite of libraries is an industrial strength alternative to
  OCaml's standard library that was developed by Jane Street, the
  largest industrial user of OCaml."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/sexplib"
  doc:
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/sexplib/index.html"
  bug-reports: "https://github.com/janestreet/sexplib/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "parsexp" {>= "v0.13" & < "v0.14"}
    "sexplib0" {>= "v0.13" & < "v0.14"}
    "dune" {>= "1.5.1"}
    "num"
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/sexplib.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.13/files/sexplib-v0.13.0.tar.gz"
    checksum: [
      "md5=d3dd8eb6f10e64e6766217bf6b57bc93"
     
  "sha256=7b0438307ff861015f96a2c8c151ad49ae483998cc94301e9ef036cf4f4cd669"
     
  "sha512=1a3a5bffc6e90933ede593b064e125a9c68934048ba963b5abda36afde54096e95fd1d9e27207d66922d5231da432897c2720e7803867dbf0d62ad640ecf447d"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, parsexp, sexplib0, dune, num, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.13.0"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare parsexp "v0.13") >= 0 && (vcompare parsexp "v0.14") < 0;
assert (vcompare sexplib0 "v0.13") >= 0 && (vcompare sexplib0 "v0.14") < 0;
assert (vcompare dune "1.5.1") >= 0;

stdenv.mkDerivation rec {
  pname = "sexplib";
  version = "v0.13.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.13/files/sexplib-v0.13.0.tar.gz";
    sha256 = "0sfn9i7wydphkqg3156ck0wlibj9mm8w3j52jrgh2qgqgwq3h13v";
  };
  buildInputs = [
    ocaml parsexp sexplib0 dune num findlib ];
  propagatedBuildInputs = [
    ocaml parsexp sexplib0 dune num ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
