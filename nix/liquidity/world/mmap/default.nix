/*opam-version: "2.0"
  name: "mmap"
  version: "1.1.0"
  synopsis: "File mapping functionality"
  description:
    "This project provides a Mmap.map_file functions for mapping files in
  memory."
  maintainer: "jeremie@dimino.org"
  authors: ["Jérémie Dimino <jeremie@dimino.org>" "Anton Bachin"]
  license: "LGPL 2.1 with linking exception"
  homepage: "https://github.com/mirage/mmap"
  doc: "https://mirage.github.io/mmap/"
  bug-reports: "https://github.com/mirage/mmap/issues"
  depends: [
    "ocaml" {>= "4.02.3"}
    "dune" {build & >= "1.6"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/mirage/mmap.git"
  url {
    src:
     
  "https://github.com/mirage/mmap/releases/download/v1.1.0/mmap-v1.1.0.tbz"
    checksum: "md5=8c5d5fbc537296dc525867535fb878ba"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml, dune,
  findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.02.3") >= 0;
assert (vcompare dune "1.6") >= 0;

stdenv.mkDerivation rec {
  pname = "mmap";
  version = "1.1.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/mmap/releases/download/v1.1.0/mmap-v1.1.0.tbz";
    sha256 = "0l6waidal2n8mkdn74avbslvc10sf49f5d889n838z03pra5chsc";
  };
  buildInputs = [
    ocaml dune findlib ];
  propagatedBuildInputs = [
    ocaml dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
