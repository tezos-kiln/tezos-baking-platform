/*opam-version: "2.0"
  name: "ppx_derivers"
  version: "1.2.1"
  synopsis: "Shared [@@deriving] plugin registry"
  description: """
  Ppx_derivers is a tiny package whose sole purpose is to allow
  ppx_deriving and ppx_type_conv to inter-operate gracefully when linked
  as part of the same ocaml-migrate-parsetree driver."""
  maintainer: "jeremie@dimino.org"
  authors: "Jérémie Dimino"
  license: "BSD3"
  homepage: "https://github.com/ocaml-ppx/ppx_derivers"
  bug-reports: "https://github.com/ocaml-ppx/ppx_derivers/issues"
  depends: [
    "ocaml"
    "dune" {build}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git://github.com/ocaml-ppx/ppx_derivers.git"
  url {
    src: "https://github.com/ocaml-ppx/ppx_derivers/archive/1.2.1.tar.gz"
    checksum: "md5=5dc2bf130c1db3c731fe0fffc5648b41"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml, dune,
  findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in

stdenv.mkDerivation rec {
  pname = "ppx_derivers";
  version = "1.2.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-ppx/ppx_derivers/archive/1.2.1.tar.gz";
    sha256 = "159vqy616ni18mn0dlv8c2y4h7mb4hahwjn53yrr59yyhzhmwndn";
  };
  buildInputs = [
    ocaml dune findlib ];
  propagatedBuildInputs = [
    ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
