/*opam-version: "2.0"
  name: "liquidity"
  version: "~dev"
  maintainer: "liquidity@ocamlpro.com"
  authors: [
    "Fabrice Lefessant <fabrice.lefessant@ocamlpro.com>"
    "Alain Mebsout <alain.mebsout@ocamlpro.com>"
    "David Declerck <david.declerck@ocamlpro.com>"
  ]
  homepage: "http://liquidity-lang.org"
  bug-reports: "https://github.com/OCamlPro/liquidity/issues"
  depends: [
    "ocaml" {= "4.06.1"}
    "bigstring"
    "calendar"
    "digestif" {>= "0.7"}
    "ezjsonm"
    "easy-format"
    "hex"
    "lwt"
    "lwt_log"
    "menhir"
    "ocamlfind"
    "ocp-build"
    "ocplib-endian"
    "ocurl"
    "sodium"
    "uri"
    "zarith"
  ]
  flags: light-uninstall
  build: make
  install: [make "install"]
  dev-repo: "git+https://github.com/OCamlPro/liquidity.git"
  url {
    src:
     
  "git+file:///home/james/proj/tezos-baking-platform/bump/liquidity#next"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml,
  bigstring, calendar, digestif, ezjsonm, easy-format, hex, lwt, lwt_log,
  menhir, findlib, ocp-build, ocplib-endian, ocurl, sodium, uri, zarith }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert stdenv.lib.getVersion ocaml == "4.06.1";
assert (vcompare digestif "0.7") >= 0;

stdenv.mkDerivation rec {
  pname = "liquidity";
  version = "~dev";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "git+file:///home/james/proj/tezos-baking-platform/bump/liquidity#next";
    sha256 = "0p43c3hhw6xgj4y2yjcw1gdnibs4asa66hg1b7z3y9s94zkmr0y4";
  };
  buildInputs = [
    ocaml bigstring calendar digestif ezjsonm easy-format hex lwt lwt_log
    menhir findlib ocp-build ocplib-endian ocurl sodium uri zarith ];
  propagatedBuildInputs = [
    ocaml bigstring calendar digestif ezjsonm easy-format hex lwt lwt_log
    menhir findlib ocp-build ocplib-endian ocurl sodium uri zarith ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" "'install'" ] ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
