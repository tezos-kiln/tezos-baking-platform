/*opam-version: "2.0"
  name: "bigarray-compat"
  version: "1.0.0"
  synopsis: "Compatibility library to use Stdlib.Bigarray when
  possible"
  maintainer: "Lucas Pluvinage <lucas.pluvinage@gmail.com>"
  authors: "Lucas Pluvinage <lucas.pluvinage@gmail.com>"
  license: "ISC"
  homepage: "https://github.com/mirage/bigarray-compat"
  bug-reports: "https://github.com/mirage/bigarray-compat/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune" {build & >= "1.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  dev-repo: "git+https://github.com/mirage/bigarray-compat.git"
  url {
    src: "https://github.com/mirage/bigarray-compat/archive/v1.0.0.tar.gz"
    checksum: [
      "md5=1cc7c25382a8900bada34aadfd66632e"
     
  "sha512=c365fee15582aca35d7b05268cde29e54774ad7df7be56762b4aad78ca1409d4326ad3b34af0f1cc2c7b872837290a9cd9ff43b47987c03bba7bba32fe8a030f"
    ]
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml, dune,
  findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare dune "1.0") >= 0;

stdenv.mkDerivation rec {
  pname = "bigarray-compat";
  version = "1.0.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/bigarray-compat/archive/v1.0.0.tar.gz";
    sha256 = "1bpmmnxb1yx72aqlbdaqfl18rgz1cq9cf6cqvnfl88mz5dfr4x0d";
  };
  buildInputs = [
    ocaml dune findlib ];
  propagatedBuildInputs = [
    ocaml dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
