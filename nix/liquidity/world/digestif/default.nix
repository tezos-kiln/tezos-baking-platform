/*opam-version: "2.0"
  name: "digestif"
  version: "0.7.1"
  synopsis: "Hashes implementations (SHA*, RIPEMD160, BLAKE2* and
  MD5)"
  description: """
  Digestif is a toolbox to provide hashes implementations in C and OCaml.
  
  It uses the linking trick and user can decide at the end to use the C
  implementation or the OCaml implementation.
  
  We provides implementation of:
   * MD5
   * SHA1
   * SHA224
   * SHA256
   * SHA384
   * SHA512
   * BLAKE2B
   * BLAKE2S
   * RIPEMD160"""
  maintainer: [
    "Eyyüb Sari <eyyub.sari@epitech.eu>"
    "Romain Calascibetta <romain.calascibetta@gmail.com>"
  ]
  authors: [
    "Eyyüb Sari <eyyub.sari@epitech.eu>"
    "Romain Calascibetta <romain.calascibetta@gmail.com>"
  ]
  license: "MIT"
  homepage: "https://github.com/mirage/digestif"
  doc: "https://mirage.github.io/digestif/"
  bug-reports: "https://github.com/mirage/digestif/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune" {build}
    "eqaf"
    "base-bytes"
    "base-bigarray"
    "fmt" {with-test}
    "alcotest" {with-test}
  ]
  depopts: ["ocaml-freestanding" "mirage-xen-posix"]
  conflicts: [
    "mirage-xen-posix" {< "3.1.0"}
    "ocaml-freestanding" {< "0.4.1"}
  ]
  build: [
    ["dune" "subst"]
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/digestif.git"
  url {
    src:
     
  "https://github.com/mirage/digestif/releases/download/v0.7.1/digestif-v0.7.1.tbz"
    checksum: "md5=f13be3170563925a2b50a4769594a31d"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml, dune,
  eqaf, base-bytes, base-bigarray, fmt ? null, alcotest ? null, findlib,
  ocaml-freestanding ? null, mirage-xen-posix ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.03.0") >= 0;
assert mirage-xen-posix != null -> !((vcompare mirage-xen-posix "3.1.0") <
  0);
assert ocaml-freestanding != null -> !((vcompare ocaml-freestanding "0.4.1")
  < 0);

stdenv.mkDerivation rec {
  pname = "digestif";
  version = "0.7.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/digestif/releases/download/v0.7.1/digestif-v0.7.1.tbz";
    sha256 = "0v3y8klrn1zirxdhp65aiy8lxybpdl7qh3hxb51p6fr0r3s3yvj0";
  };
  buildInputs = [
    ocaml dune eqaf base-bytes base-bigarray ]
  ++
  stdenv.lib.optional
  doCheck
  fmt
  ++
  stdenv.lib.optional
  doCheck
  alcotest
  ++
  [
    findlib ]
  ++
  stdenv.lib.optional
  (ocaml-freestanding
  !=
  null)
  ocaml-freestanding
  ++
  stdenv.lib.optional
  (mirage-xen-posix
  !=
  null)
  mirage-xen-posix;
  propagatedBuildInputs = [
    ocaml eqaf base-bytes base-bigarray ]
  ++
  stdenv.lib.optional
  doCheck
  fmt
  ++
  stdenv.lib.optional
  doCheck
  alcotest
  ++
  stdenv.lib.optional
  (ocaml-freestanding
  !=
  null)
  ocaml-freestanding
  ++
  stdenv.lib.optional
  (mirage-xen-posix
  !=
  null)
  mirage-xen-posix;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'subst'" ] [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ]
    (stdenv.lib.optionals doCheck [
      "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ])
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
