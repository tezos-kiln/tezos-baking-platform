# Using Tezos Baking Platform

To use Tezos via Baking Platform, start by running the following Nix command. It should take less time to run if you have [set up nix caching](https://gitlab.com/tezos-kiln/tezos-baking-platform#setting-up-nix-caching-recommended). Replace `<network>` with your desired Tezos network, i.e. zeronet, alphanet, mainnet.

```
nix-build -A tezos.<network>.kit --no-out-link
```

Once that `nix-build` compeletes, you can use the `tezos-client`, `tezos-node`, and other Tezos binaries as you normally would by prefixing your commands with `$(nix-build -A tezos.<network>.kit --no-out-link)/bin/`.

For example, if you'd like to run a Tezos node, use the following command, replacing `<network>` with the network you chose above.

```
$(nix-build -A tezos.<network>.kit --no-out-link)/bin/tezos-node run --rpc-addr 127.0.0.1:8732 --data-dir ~/.<network>-tezos-node
```

The `--rpc-addr` option opens the RPC port the node listens on. The `--data-dir` is where identity and chain data are stored. You'll need to use the `--data-dir` command so `tezos-node` knows where to find the correct directory.

To see all available binaries, run `ls $(nix-build -A tezos.<network>.kit --no-out-link)/bin/`, where `<network>` is the network used during your `nix-build`.

# Updating the Tezos Core Code

If you wish to use newer or forked versions of the upstream Tezos Core code, a script is provided to assist with this.  First make sure that the thunk in `tezos/_network_` points to a git repository and branch containing the code you wish to use; see the [obelisk](https://github.com/obsidiansystems/obelisk/) for details.  Then run

```
./bump.sh _network_
```

The script may require you to answer some prompts.

This should do everything required; however, make sure to run a test build of the selected network afterward.

# Adding a New Tezos Network

If you wish to add a completely separate Tezos network in order to enable, say, off-network testing of a possible protocol update, perform the following setup:

* Create a thunk at `tezos/_new-network-name_` pointing to the appropriate version of the Tezos Core code.

* Place a derivation to build an appropriate version of opam at `nix/tezos/_new-network-name_/opam.nix`.

* To the definition of `tezos` in `default.nix`, add a line reading:

```
_new-network-name_ = mk /_new-network-name_;
```

* Run:

```
./bump.sh _new-network-name_
```
