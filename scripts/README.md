# Test Scripts

`run-accusation-tests.sh` runs a test of Kiln's detection of accusations in a network sandbox.
It is standalone and should not need to be run in a special context.  The user should follow
any prompts to set up bakers or other configuration; after following the instructions they
should press `q<ENTER>` to proceed.  When each test finishes; observe the result at
http://localhost:8086/ and press `q<ENTER>` to end the test.  There are line command options
to run only one of the test scenarios or to use a specific Kiln docker image; see the output
of the `--help` option.

`fast-baking-sandbox.sh` runs a `nix-shell` command to enter into a Nix sandbox customized with
settings appropriate for baking testing (especially for the Ledger) by getting baking to move
quickly.

Within this Nix sandbox, general set up can be done with
`sandbox-nodes.sh`, which sets up a fresh Tezos genesis block and nodes
(killing all existing nodes) but does not run bakers. Bakers can be then
run with `bootstrap-baking.sh` which is not a script in this directory
but a command put in the path by Nix.

`zeronet-node.sh` creates a new node configuration if it hasn't been created, and then starts
a Zeronet node by querying what to connect to.

`theworks-more.sh` is a general test of Tezos functionality without Ledger included.
