#!/usr/bin/env bash

set -eu

AUTHSTUFF="$(declare -p "${!SSH_@}")"

PWD="$(pwd)"

if [ ! -d "bump/opam-root" ]; then
  mkdir -p bump
  nix-shell --pure -p bubblewrap perl m4 curl unzip git rsync 'callPackage nix/opam_2_0_0.nix {}' --run "$AUTHSTUFF"'; opam init --root=bump/opam-root -n --bare'
fi

export OPAMROOT="$PWD"/bump/opam-root

if [ ! -d "bump/opam-nixify" ]; then
  mkdir -p bump
  git clone https://github.com/obsidiansystems/opam-nixify.git bump/opam-nixify
fi

if [ ! -d "bump/opam-nixify/_opam" ]; then
  mkdir -p bump
  nix-shell --pure -p bubblewrap perl m4 curl unzip git rsync 'callPackage nix/opam_2_0_0.nix {}' --run "$AUTHSTUFF"'; opam switch --root=bump/opam-root create bump/opam-nixify 4.06.1'
fi

nix-shell --pure -p bubblewrap perl m4 curl unzip git rsync 'callPackage nix/opam_2_0_0.nix {}' --run "$AUTHSTUFF"'; export OPAMROOT="$(pwd)/bump/opam-root"; cd bump/opam-nixify && eval $(opam env --switch=. --set-switch) && opam install -w opam-nixify'

rm -rf bump/liquidity
ob thunk update liquidity
mkdir -p bump/liquidity
cp -r liquidity bump/
ob thunk unpack bump/liquidity
NIX_PREFETCH_GIT="$(nix-build nix/nixpkgs.nix -A nix-prefetch-scripts)/bin/nix-prefetch-git"
$NIX_PREFETCH_GIT --url "$(git config -f bump/liquidity/.gitmodules submodule.tezos.url)" \
	          --rev "$(cd bump/liquidity && git rev-parse :tezos)" \
		  > tezos/for-liquidity/git.json
nix-shell --pure -p git curl --run "$AUTHSTUFF"'; cd bump/liquidity && make clone-tezos'
nix-shell --pure -p bubblewrap perl m4 curl unzip git rsync which pkgconfig libev gmp hidapi libsodium 'callPackage nix/liquidity/opam.nix {}' --run "$AUTHSTUFF"'; cd bump/liquidity; export OPAMROOT="$(pwd)/.opam"; opam init -n --bare; opam switch create . 4.06.1; eval $(opam env --switch=. --set-switch); make build-deps'
rm -rf nix/liquidity/world
export OPAMROOT="$PWD/bump/liquidity/.opam"
bump/opam-nixify/_opam/bin/opam-nixify --switch "$PWD/bump/liquidity" --settings=liquidity.nixpam
TARGET_NEEDED=$(nix-shell --pure -p bubblewrap perl m4 curl unzip git rsync 'callPackage nix/opam_2_0_0.nix {}' --run "$AUTHSTUFF"'
  export LC_ALL=C
  cd bump/liquidity
  opam show --root=.opam --switch=. -f depends: ./opam \
    | perl -e '\''
       while ($x=<>) {
         $x =~ s/"([^"]*)"(?:\s+[{](.*?)[}])?\s*/
                 (grep { $_ eq "with-test" } split \/\s*&\s*\/, $2) ? "" : "$1\n"/eg;
         print $x;
       }'\'' \
    | sort | uniq \
    | join -v1 - <(echo liquidity) \
    | perl -pe '\''s/^hidapi\n/ocaml-hidapi\n/; s/^ocamlfind\n/findlib\n/; '\''
  '
)
DEPENDS_COMMAS=""
DEPENDS_SPACES=""
for i in $TARGET_NEEDED; do
  DEPENDS_COMMAS="$DEPENDS_COMMAS, $i"
  DEPENDS_SPACES="$DEPENDS_SPACES $i"
done
export DEPENDS_COMMAS
export DEPENDS_SPACES
VERSION="$(grep -Proh 'let output_version = "\K.*(?=")' bump/liquidity/tools)"
export VERSION
perl -pe 's/, __DEPENDS_COMMAS__/$ENV{DEPENDS_COMMAS}/g; s/ __DEPENDS_SPACES__/$ENV{DEPENDS_SPACES}/g; s/__VERSION__/$ENV{VERSION}/g;' nix/liquidity/default.nix.template >nix/liquidity/default.nix
